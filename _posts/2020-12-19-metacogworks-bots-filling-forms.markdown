---
layout: post
title:  "Metacogworks: Bots Filling Forms"
date:   2020-12-19 21:48:14 -0300
categories: posts
---

## Bots here, bots there, bots everywhere

I've found it quite interesting to go into MetacogWork's database from time to time and look at new users registering for the service. Overwelmingly it is bots, yet it is interesting just to see the number of them coming in and poking around.

It isn't a ton, so the impact of dummy accounts being made isn't costing me anything. Right now since the last time I cleared them out, there are about 23 bot accounts sitting idle. These are obviously bots as they are well filled out but with obvious names that don't sync up with the other registration details.

### The need for a verification step on new accounts in web apps

By watching some of the activity, it really shows that one should probably have an email or SMS verification step on new accounts for web apps just so that bots don't go in and potentially scrape semi-private information if an app has internal forums or groups. MetacogWorks doesn't have that right now and is very much restricted to only one's own institution or user-rights level, so if anything, having exposure of the platform's internals is only positive. 

I haven't added an extra verification step as I like having a one-click registration process. However this has me adding a todo-note to add in a verification step in the registration process for the future once MetacogWorks nears its V1.0 release.

### MetacogWorks

<img src="/assets/img/metacogworks.png" style="margin-top: 20px; margin-bottom: 20px" alt="Picture of Metacogworks Website">

MetacogWorks is the only complete assessment tool for conducting metacognitive evaluations on the web. Answers to standardized assessments can be used by teachers to construct individualized learning plans, and as a result enable students reach their best potential.

Right now institutions can create an account for free while the service is being built out. Take advantage of the beta period by sending in feedback! Feedback sent in has a high chance of steering the platform's future construction.

Visit: [Metacogworks](http://metacogworks.com)
