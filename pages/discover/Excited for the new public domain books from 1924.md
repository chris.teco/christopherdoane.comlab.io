---
date: 2020-12-23T22:01:33+01:00
title: Excited for the new public domain books of 1924
modified: 2020-12-23T22:08:21+01:00
---

It's exciting to finally see the copywrite extended works now releasing out to the public domain. We had a few last year as well, but the implications are great for our creatives. This always sparks a wave of new movie making and derivative works as now you can incorporate known-to-work plots directly into new material without having to hide it in some form of homage.

https://bookriot.com/2020-public-domain-books/