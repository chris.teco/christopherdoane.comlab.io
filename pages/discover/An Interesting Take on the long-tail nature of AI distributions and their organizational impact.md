---
date: 2020-12-23T21:23:47+01:00
modified: 2020-12-23T21:28:22+01:00
title: An Interesting Take on the long-tail nature of AI distributions and their organizational impact
---

This is a great take on the pitfalls of many AI training datasets and how they impact the economic viability and stability of many companies working in the AI space. It really boils down to the long-tail nature of many of these datasets.


https://a16z.com/2020/08/12/taming-the-tail-adventures-in-improving-ai-economics/