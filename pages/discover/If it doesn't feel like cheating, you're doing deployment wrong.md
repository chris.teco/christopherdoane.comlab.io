---
date: 2020-12-27T13:05:00+01:00
modified: 2020-12-27T13:07:00+01:00
title: If it doesn't feel like cheating, you're doing deployment wrong
---

When it comes to deployment, starting simple is almost always best. Kubernetes is wonderful and I love that so many tools are standardizing the k8s API, but even a set of bash scripts can get you quite far.

https://blog.markjgsmith.com/2020/11/13/robust-nodejs-deployment-architecture.html