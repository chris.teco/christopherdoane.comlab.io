---
date: 2020-12-23T21:49:54+01:00
title: The UN Carbon Credit Scheme emailed me yesterday
modified: 2020-12-23T21:58:47+01:00
---

Apparently I am 1 of only 1409 purchasers of carbon credits via their platform directly. Now I hope (and have seen) that a number of these "contributors" are actually Non-profits people go through as middle men. I was still surprised though that the the number of separate contributers, let's call them "sources" was only 1409, amounting to just over a million tonnes of offsetted CO2 emissions. That's nothing on a platform that I thought was bigger! Around 500 credits of those 1+ million were me last year! Gosh I thought more people did these sort of things.

If you don't already know about it, check out the platform in the following link. If you search for "Gold Standard" you can sometimes directly purchase those certified credits as well. Credits shouldn't be your only way to contribute, but can be a nice way to complement any other action you are taking.


<img src="/assets/img/orders.png" style="margin-top: 20px; margin-bottom: 20px" alt="Picture of Carbon Credit Orders">

Visit: [United Nations Carbon Marketplace](https://offset.climateneutralnow.org/)