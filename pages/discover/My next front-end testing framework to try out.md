---
date: 2020-12-23T21:44:33+01:00
modified: 2020-12-23T22:13:14+01:00
title: My next front-end testing framework to try out
---

I have a number of collegues who have used Playwright and are impressed by it.

Reading online, I get the feeling that Cypress also scratches a similar itch, with a number of people really raving about their experience. Can't wait to dabble in it and see if it reduces pain points. Getting automated alarms when things don't render as expected are a great way of doing light-integration testing for detecting when internal or dependent API's change without warning.

https://www.cypress.io/