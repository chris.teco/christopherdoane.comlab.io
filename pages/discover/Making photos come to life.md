---
date: 2021-04-05T21:32:12+01:00
modified: 2021-04-05T21:32:12+01:00
title: Making photos come to life
---

I stumbled upon MyHeritage Deep Nostalgia, a service to make still photographs move as if they were short film clips.
While the demos are little wonky, I can see huge potential in this kind of tech, and shows one visual use of DL.
I could see this tech being interpretted as controversial, like digging up graves, however it does bring us closer to a world of having motion photographs like one saw as a child in Harry Potter.

https://www.myheritage.com/deep-nostalgia
