---
date: 2020-12-22T13:21:28+01:00
modified: 2020-12-22T13:36:13+01:00
title: The 60s vision of vertical mobility
---

Interesting concept to have a drive-up vertically-parking spiral spots near or in major cities to permit a mobile workforce. A fun thought experiment to read about in history.

https://99percentinvisible.org/article/mobile-home-skyscrapers-elusive-dream-vertical-urban-trailer-parks/