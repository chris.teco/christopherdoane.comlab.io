---
date: 2020-12-27T10:36:44+01:00
modified: 2020-12-27T10:39:37+01:00
title: Biological extinction chain reactions
---

Quite scary to see the rapid die-off of birds and other species due to the changing climate and reduction of our bug population this year. One can only pull out the rug from so many rooms and dump on more weight before the floor falls in.

https://www.theguardian.com/environment/2020/dec/26/mass-die-off-of-birds-in-south-western-us-caused-by-starvation-aoe