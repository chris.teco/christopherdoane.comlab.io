---
layout: page
title: Posts
permalink: /posts/
menu: true
---


<ul class="post-list">
    {% for post in site.posts %}
      {% for pc in post.categories %}
        {% if pc == "posts" %}
          <li>
            <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>

            <h2>
              <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
            </h2>
          </li>
        {% endif %}
      {% endfor %}
    {% endfor %}
</ul>

<p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p>