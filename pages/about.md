---
layout: page
title: About Christopher
permalink: /about/
menu: true
modified: 2020-04-05T19:21:44+01:00
---

**Welcome to my small corner of the internet!**

<img src="/assets/img/index.png" align="right" style="margin-top: 20px; margin-bottom: 20px" alt="Picture of Christopher">

I’m a knowledge- and collaboration-hungry software engineer with several years of professional paid experience in various engineering roles.

I have a passion for solving problems with large societal and organizational impact. My strengths lie in the intersection of product development, software engineering, technical sales communication and technical org management - all while putting an approachable human face to deeply technical and organizational decisions.

I have both team-lead and global experience bridging NY, Silicon Valley and Sweden.

Currently employed at Mighty Networks, probably the most impressive value-driven community-building startups in Silicon Valley 🎉

<img src="/assets/img/smaller-groupcrop.jpg" align="left" height="300px" style="margin-right: 30px; margin-left: 20px; margin-top: 10px; margin-bottom: 10px" alt="Picture of Christopher">

Previously employed as a Software Engineer at Telavox, where the focus was on integrations, and scaleable tech. During the pandemic I knocked out a masters from Georgia Tech on the side as well. At Telavox I was promoted to Team Lead to work on a new customer-facing product offering in the low-code web-integrations space.

Prior to Telavox, I worked as a software engineer and Team Lead at ArcCore (now Vector) where I was on the products team of 5 other engineers building and maintaining Arctic Studio. Other responsibilities included workload planning and a large development responsibility in their new (at that time) Adaptive AUTOSAR code-generation products. This included product demos on-site at customers.

During bachelors studies I worked as a Software Developer at AgriCam. I also volunteering with Engineers without Borders in a Ghana water project. I have a long history of FFA involvement, speaking competitions and running an IT consultancy called Doane's Design during pre-university years.

Passionate about causes that touch environmental protection and minimizing exploitation. ❤️
If you have a project targeting sustainability or positive human development, don't hesitate to contact if you are looking for a co-founder.


This site is a place to collect posts (see POSTS tab) and curate a discovery feed (see DISCOVER tab) of other sites that I've discovered during the week. Want to talk? Feel free to [send me an email](mailto:webcontact@christopherdoane.com)

Anything written here is my own opinion and does not reflect the opinions of my employer(s). My identity, opinions and experience changes slightly from year to year as I learn more, so some of this is probably already out of date. To be honest, I'm horrible at prioritizing this website- but hey, life is busy! :D

Have a great day!
