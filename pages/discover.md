---
layout: post
title: Discover
permalink: /discover/
menu: true
---

A curated feed of interesting 3rd party sites, articles, happenings and more.


<ul class="post-list">
    {% for page in site.pages %}
      {% if page.menu == null %}
          <li>
            <span class="post-meta">{{ page.date | date: "%b %-d, %Y" }}</span>

            <h2>
              <a class="post-link" href="{{ page.url | prepend: site.baseurl }}">{{ page.title }}</a>
            </h2>
            
          </li>
      {% endif %}
    {% endfor %}
</ul>